import bcrypt # для кодирования и раскодирования пароля
import jwt # для кодирования и раскодирования iwt токенов
import datetime # для определения времени
from secret_key import SECRET_KEY # секретный ключ


TIME_TOKEN = 30 # Время работы токена в секундах, можно поменять на другое значение


# шифруем пароль
def hash_password(user_password):
    pwd_bytes = user_password.encode('utf-8')
    salt = bcrypt.gensalt()
    return bcrypt.hashpw(password=pwd_bytes, salt=salt)


# проверяем пароль, в ответ выдаём True или False
# user_password - пароль пользователя, который он вводит при входе в сервис
# db_password - пароль в зашифрованном виде из базы данных
def test_password(user_password, db_password):
    pwd_bytes = user_password.encode('utf-8')
    return bcrypt.checkpw(password = pwd_bytes,
        hashed_password = db_password)


# кодируем jwt-токен
def access_token(user_login):
    payload = {
    'exp' : datetime.datetime.now(datetime.UTC) + datetime.timedelta(seconds=TIME_TOKEN),
    'iat' : datetime.datetime.now(datetime.UTC),
    'scope' : 'access_token',
    'user_login' : user_login}
    return jwt.encode(payload, SECRET_KEY, algorithm="HS256")


# декодируем jwt-токен
def decode_token(token):
    return jwt.decode(token, SECRET_KEY, algorithms="HS256")


