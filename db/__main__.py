import sqlalchemy as db

# Создаём движок
# sqlite - название диалекта БД
# SalaryDataBase.db - название хранилища и расширение db
engine = db.create_engine('sqlite:///SalaryDataBase.db')

# Запускаем движок
conn = engine.connect()

metadata = db.MetaData()

# Опишем таблицу: id, логин, пароль, з/п, дата повышения, токен.
users = db.Table('users', metadata, 
    db.Column('user_id', db.Integer, primary_key=True),
    db.Column('login', db.String),
    db.Column('password', db.String),
    db.Column('salary', db.String),
    db.Column('salary_data_up', db.String),
)

# Создаём объёкт таблицы
metadata.create_all(engine)


"""# Выбираем данные из таблицы
result = conn.execute(users.select())

# Печатаем результат
for row in result:
    print(row)"""
