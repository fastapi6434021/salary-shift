from fastapi import FastAPI, Body, Request
from fastapi.responses import FileResponse, JSONResponse

from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import DeclarativeBase, sessionmaker

import core

app = FastAPI()

# строка подключения к БД
sqlite_database = "sqlite:///db/SalaryDataBase.db"
engine = create_engine(sqlite_database, connect_args={"check_same_thread": False})

# создаем модель, объекты которой будут храниться в бд
class Base(DeclarativeBase): pass
class Person(Base):
    __tablename__ = "users"

    user_id = Column(Integer, primary_key=True)
    login = Column(String)
    password = Column(String)
    salary = Column(String)
    salary_data_up = Column(String)
 
# создаем таблицы
Base.metadata.create_all(bind=engine)
 
# создаем сессию подключения к бд
SessionLocal = sessionmaker(autoflush=False, bind=engine)
db = SessionLocal()


"""Страница входа в сервис login.html"""
@app.get("/")
async def root():
    return FileResponse("public/login.html")

"""Выдача куки в ответ на комбинацию логин-пароль на странице login"""
@app.post("/login")
def login(data = Body()):
    user_login = data["user_login"]
    user_password = data["user_password"]
    user = db.query(Person).filter(Person.login==user_login).first()

    # Пользователь существует?
    if not user:
        return {"message": f"Введён неверный логин."}
    
    # Пароль правильный?
    if core.test_password(user_password, user.password) != True:
        return {"message": f"Введён неверный пароль."}

    # Если пароль правильный, то генерируем токен
    access_token = core.access_token(user.login)
    # Создаём куку
    content = {"message": "Come to the dark side, we have cookies"}
    response = JSONResponse(content=content)
    response.set_cookie(key='access_token', value=access_token, httponly=True)
    return response


"""Cтраница информации о з/п и дате повышения"""
@app.get("/salary")
async def root():
    return FileResponse("public/salary.html")

"""Проверка куки и выдача информации о з/п и дате ближайшего повышения"""
@app.post("/salary")
def test_cookies(request: Request):
    access_token = request.cookies.get('access_token')

    # Проверяем, что токен присутствует в куках
    if not access_token:
        return {"message": f"Авторизируйтесь на странице авторизации."}

    # Проверяем истёк токен или нет: значение TIME_TOKEN в core
    try:
        cookie_payload = core.decode_token(access_token)
        user_login = cookie_payload['user_login']
    except:
        return {"message": f"Авторизируйтесь на странице авторизации."}

    user = db.query(Person).filter(Person.login==user_login).first()

    # Если по какой-то причине пользователь не найден, выдаём ошибку
    if not user:
        return {"message": f"Ошибка авторизации."}
    else:
        mes = f"Текущая заработная плата {user.salary} руб."
        mes += f"\nДата ближайшего повышения {user.salary_data_up} г."
        return {"message": mes}


"""Регистрация нового пользователя в БД на странице signup.html"""
@app.get("/signup")
async def root():
    return FileResponse("public/signup.html")

"""Регистрация нового пользователя и шифрование пароля"""
@app.post("/signup")
def signup(data = Body()):
    user_login = data["user_login"]
    user_password = data["user_password"]
    user_salary = data["user_salary"]
    user_salary_data_up = data["user_salary_data_up"]
    
    # Проверяем нет ли такого же логина пользователя уже в системе
    if not db.query(Person).filter(Person.login==user_login).first():
        pass
    else:
        return {"message": f"Аккаунт уже существует."}

    # шифруем пароль и сохраняем пользователя в БД
    try:
        hashed_password = core.hash_password(user_password)
        user = Person(login=user_login, password=hashed_password,
            salary=user_salary, salary_data_up=user_salary_data_up)
        db.add(user)
        db.commit()
        return {"message": f"Аккаунт {user.login} зарегистрирован."}
    except:
        return {"message": f"Не удалось зарегистрировать пользователя."}